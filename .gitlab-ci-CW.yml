image: docker:latest

variables:
  DOCKER_DRIVER: overlay
#  GIT_DEPTH: 1
  BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  COMMIT: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  NIGHTLY: $CI_REGISTRY_IMAGE:nightly
  TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  LAL_DIR: $CI_PROJECT_DIR/opt/lalsuite
  PKG_ARGS: --disable-all-lal --enable-lalpulsar --enable-lalframe --enable-lalxml --enable-python --enable-cfitsio --enable-swig --enable-doxygen
stages:
  - level0
  - level1
  - level2
  - level3
  - level4
  - release
  - nightly

before_script:
  - ulimit -S -c 0
  - export VERBOSE="true"
  - export PATH=/usr/lib/ccache:/opt/local/libexec/ccache:$PATH
  - export CCACHE_DIR=${PWD}/ccache
  - export PKG_CONFIG_PATH=${LAL_DIR}/lib/pkgconfig
  - mkdir -p opt/lalsuite

cache:
  key: $CI_JOB_NAME
  paths:
    - ccache

level0:lal:
  image: ligo/lalsuite-dev:jessie
  stage: level0
  script:
    - cd lal
    - ./00boot
    - ./configure --enable-swig --enable-doxygen --prefix=${LAL_DIR}
    - make dist
    - tar xf lal-*.tar.xz
    - cd lal-*
    - ./configure --enable-swig --enable-doxygen --prefix=${LAL_DIR}
    - make -j4 V=1
    - make -j4 V=1 VERBOSE=1 check
    - make -j4 install
    - make -j4 install-html
  artifacts:
    expire_in: 3h
    paths:
      - opt/lalsuite
  only:
    - pushes
    - schedules

level1:lalframe:
  image: ligo/lalsuite-dev:jessie
  stage: level1
  script:
    - cd lalframe
    - ./00boot
    - ./configure --enable-swig --enable-doxygen --prefix=${LAL_DIR}
    - make dist
    - tar xf lalframe-*.tar.xz
    - cd lalframe-*
    - ./configure --enable-swig --enable-doxygen --prefix=${LAL_DIR}
    - make -j4 V=1
    - make -j4 V=1 VERBOSE=1 check
    - make -j4 install
    - make -j4 install-html
  artifacts:
    expire_in: 3h
    paths:
      - opt/lalsuite
  dependencies:
    - level0:lal
  only:
    - pushes
    - schedules

level1:lalxml:
  image: ligo/lalsuite-dev:jessie
  stage: level1
  script:
    - cd lalxml
    - ./00boot
    - ./configure --enable-swig --enable-doxygen --prefix=${LAL_DIR}
    - make dist
    - tar xf lalxml-*.tar.xz
    - cd lalxml-*
    - ./configure --enable-swig --enable-doxygen --prefix=${LAL_DIR}
    - make -j4 V=1
    - make -j4 V=1 VERBOSE=1 check
    - make -j4 install
    - make -j4 install-html
  artifacts:
    expire_in: 3h
    paths:
      - opt/lalsuite
  dependencies:
    - level0:lal
  only:
    - pushes
    - schedules

level2:lalpulsar:
  image: ligo/lalsuite-dev:jessie
  stage: level2
  script:
    - cd lalpulsar
    - ./00boot
    - ./configure --enable-cfitsio --enable-swig --enable-doxygen --prefix=${LAL_DIR}
    - make dist
    - tar xf lalpulsar-*.tar.xz
    - cd lalpulsar-*
    - ./configure --enable-cfitsio --enable-swig --enable-doxygen --prefix=${LAL_DIR}
    - make -j4 V=1
    - make -j4 V=1 VERBOSE=1 check
    - make -j4 install
    - make -j4 install-html
  artifacts:
    expire_in: 3h
    paths:
      - opt/lalsuite
  dependencies:
    - level1:lalxml
  only:
    - pushes
    - schedules

level4:lalapps:
  image: ligo/lalsuite-dev:jessie
  stage: level4
  script:
    - cd lalapps
    - ./00boot
    - ./configure ${PKG_ARGS} --prefix=${LAL_DIR}
    - make dist
    - tar xf lalapps-*.tar.xz
    - cd lalapps-*
    - ./configure ${PKG_ARGS} --prefix=${LAL_DIR}
    - make -j4 V=1
    - make -j4 V=1 VERBOSE=1 check
    - make -j4 install
    - make -j4 install-html
  artifacts:
    expire_in: 3h
    paths:
      - opt/lalsuite
  dependencies:
    - level2:lalpulsar
    - level1:lalframe
  only:
    - pushes
    - schedules

# release a debian docker image upon every tag in lalsuite
docker:tags:
  stage: release
  before_script: []
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --pull -t $TAG .
    - docker push $TAG
  dependencies:
    - level4:lalapps
  only:
    - tags

# release a debian docker image of each nightly build
docker:nightly:
  stage: nightly
  before_script: []
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --pull -t $NIGHTLY .
    - docker push $NIGHTLY
  dependencies:
    - level4:lalapps
  only:
    - master@lscsoft/lalsuite
    - schedules
  except:
    - pushes

nightly:top-level:el7:
  image: ligo/lalsuite-dev:el7
  stage: nightly
  script:
    - ./00boot
    - ./configure --enable-swig --enable-doxygen
    - make -j4 distcheck
  only:
    - schedules
    - web

# do not perform a jessie build because we are doing one nightly via
# the normal CI parellel build process

nightly:top-level:stretch:
  image: ligo/lalsuite-dev:stretch
  stage: nightly
  script:
    - ./00boot
    - ./configure --enable-swig --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:top-level:trusty:
  image: skymoo/ligo-lalsuite-dev:trusty
  stage: nightly
  script:
    - ./00boot
    - ./configure --disable-swig --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:top-level:xenial:
  image: skymoo/ligo-lalsuite-dev:xenial
  stage: nightly
  script:
    - ./00boot
    - ./configure --disable-swig --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:top-level:buster:
  image: skymoo/ligo-lalsuite-dev:buster
  stage: nightly
  dependencies: []
  script:
    - ./00boot
    - ./configure --enable-swig-python --enable-doxygen
    - make -j4 distcheck
  only:
    - schedules
    - web
  allow_failure: true

nightly:mpi:jessie:
  image: ligo/lalsuite-dev:jessie
  stage: nightly
  dependencies: []
  script:
    - ./00boot
    - ./configure --enable-swig --enable-doxygen --enable-mpi
    - make -j4 distcheck
  only:
    - schedules
    - web

nightly:openmp:jessie:
  image: ligo/lalsuite-dev:jessie
  stage: nightly
  dependencies: []
  script:
    - ./00boot
    - ./configure --enable-swig --enable-doxygen --enable-openmp
    - make -j4 distcheck
  only:
    - schedules
    - web

nightly:python3:stretch:
  image: ligo/lalsuite-dev:stretch
  stage: nightly
  script:
    - ./00boot
    - PYTHON=python3 ./configure --enable-swig --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:macos:
  tags:
    - macos
  stage: nightly
  script:
    - ./00boot
    - ./configure --enable-swig --enable-doxygen
    - make -j4
    - make -j4 check
  dependencies: []
  only:
    - schedules
    - web

pages:
  image: ligo/lalsuite-dev:stretch
  stage: nightly
  script:
    - ./00boot
    - ./configure --enable-doxygen --prefix=${LAL_DIR}
    - make -j4 install-html
    - cp -r ${LAL_DIR}/share/doc public/
    - pushd public
    - cp lalsuite/index.html index.html
    - sed -i 's/..\/lal/lal/g' index.html
    - popd
  dependencies: []
  artifacts:
    paths:
      - public
  only:
    - master@lscsoft/lalsuite
  except:
    - pushes
